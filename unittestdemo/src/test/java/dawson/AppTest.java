package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void echoMethodShouldReturnSameNumber(){
        //just checking if I could use var to reference the app object
        var test = new App();
        assertEquals("this is to check wether the method echo returns the same value it is given",5,test.echo(5));
    }
    @Test
    public void oneMoreMethodReturnWrong(){
        var test = new App();
        assertEquals("The result was not plus one of the value given to the method, try again",3,test.oneMore(2));
    }

    
}
