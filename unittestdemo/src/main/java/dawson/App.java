package dawson;

/**
 * Hello world!
 *
 */
public class App 
{   
    public int echo(int value){
        return value;
    }
    public int oneMore(int x){
        return x;
    }
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
